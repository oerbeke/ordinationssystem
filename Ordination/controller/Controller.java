package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Iterator;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class Controller {
	private Storage storage;
	private static Controller controller;

	public Controller() {
		storage = Storage.getStorage();
	}

	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}

	public static Controller getTestController() {
		return new Controller();
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke. Pre: startDen, slutDen, patient og laegemiddel er
	 * ikke null
	 *
	 * @return opretter og returnerer en PN ordination.
	 */

	public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,double antal) {

		if (startDen.isAfter(slutDen)) {
			try {
				throw new IllegalArgumentException("IllegalArgumentExceptionPNOrdination");
			} catch (IllegalArgumentException e) {
				System.out.println("IllegalArgumentException: StartDate is after EndDate");
				throw e;
			}
		}
		
		if (antal > 0) {
			PN pn = new PN(patient, startDen, slutDen, antal);
			pn.setLaegemiddel(laegemiddel);
			patient.addOrdination(pn);
			return pn;
		
		} else {
			System.out.println("ERROR: antal < 0");
			return null;
		}
		
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		if (startDen.isAfter(slutDen) && slutDen.isBefore(startDen)) {
    		throw new IllegalArgumentException("Start datoen må ikke være efter slut datoen");
    	} 

    	DagligFast nyOrdination = new DagligFast(startDen, slutDen, patient);
		nyOrdination.setLaegemiddel(laegemiddel);
		nyOrdination.createDosis(morgenAntal, middagAntal, aftenAntal, natAntal);
		patient.addOrdination(nyOrdination);
    	System.out.println("DagligFast er nu blevet oprettet" + " patient navn: " + patient.getNavn() + " antal ordinationer: " + patient.getOrdinationer().size());
    	// returnerer den nye ordination.
        return nyOrdination;
	}

	/**
	 * Opretter og returnerer en DagligSk�v ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke.
	 * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige kastes
	 * ogs� en IllegalArgumentException.
	 *
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder) {

		// checks if startDate is after the given endDate
		if (startDen.isAfter(slutDen)) {
			throw new IllegalArgumentException("IllgealArgumentException: StartDate is after EndDate");
		}

		// checks if time displays matches number of units
		if (klokkeSlet.length != antalEnheder.length) {
			throw new IllegalArgumentException("Klokkeslet skal passe med antal enheder");
		}

		// creates an new instance of DagligSkaev
		DagligSkaev newDagligSkaev = new DagligSkaev(startDen, slutDen, patient);
		newDagligSkaev.setLaegemiddel(laegemiddel); // sets laegemiddel

		// iteration over each unit to create doses
		for (int i = 0; i < antalEnheder.length; i++) {
			newDagligSkaev.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}

		return newDagligSkaev;
	}

	/**
	 * En dato for hvorn�r ordinationen anvendes tilf�jes ordinationen. Hvis
	 * datoen ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException Pre: ordination og dato er ikke null
	 */
	public void ordinationPNAnvendt(PN ordination, LocalDate dato) {
		if (ordination == null || dato == null)
			return;

		if (ordination.getStartDato().isAfter(dato) && ordination.getSlutDen().isBefore(dato)) {
			ordination.givDosis(dato);
		} else {
			throw new IllegalArgumentException("Datoen er ikke indenfor ordinationens gyldighedsperiode");
		}
	}

	/**
	 * Den anbefalede dosis for den p�g�ldende patient (der skal tages hensyn
	 * til patientens v�gt). Det er en forskellig enheds faktor der skal anvendes,
	 * og den er afh�ngig af patientens v�gt. Pre: patient og l�gemiddel er
	 * ikke null
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double result;
		if (patient.getVaegt() < 25) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (patient.getVaegt() > 120) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		} else {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return result;
	}

	/**
	 * For et givent vaegtinterval og et givent laegemiddel, hentes antallet af
	 * ordinationer. Pre: laegemiddel er ikke null
	 */
	public int antalOrdinationerPrVaegtPrLaegemiddel(double vaegtStart, double vaegtSlut, Laegemiddel laegemiddel) {
		// Creates an Iterator on all patienter in storage
		Iterator<Patient> patients = controller.getAllPatienter().iterator();
		int antal = 0;

		// checks precondition
		if (laegemiddel != null) {
			// Iteration over each patient in storage
			while (patients.hasNext()) {
				Patient patient = patients.next(); // grabs each patient
				// checks if patient weight meet weight interval requirements
				if (patient.getVaegt() > vaegtStart && patient.getVaegt() < vaegtSlut) {
					// Iterator on each patient's ordinations
					Iterator<Ordination> ordinations = patient.getOrdinationer().iterator();
					while (ordinations.hasNext()) {
						Ordination ordination = ordinations.next();
						// if the current ordinations medic ordination matches the input laegemiddel,
						// we increment antal
						if (ordination.getLaegemiddel().equals(laegemiddel)) {
							antal++;
						}
					}
				}
			}

		}
		return antal;
	}

	public List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger f�r en slutDato.
	 *
	 * @return true hvis startDato er f�r slutDato, false ellers.
	 */
	private boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		boolean result = true;
		if (slutDato.compareTo(startDato) < 0) {
			result = false;
		}
		return result;
	}

	public Patient opretPatient(String cpr, String navn, double vaegt) {
		Patient p = new Patient(cpr, navn, vaegt);
		storage.addPatient(p);
		return p;
	}

	public Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
			double enhedPrKgPrDoegnTung, String enhed) {
		Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
				enhed);
		storage.addLaegemiddel(lm);
		return lm;
	}

	public void createSomeObjects() {
		opretPatient("121256-0512", "Jane Jensen", 63.4);
		opretPatient("070985-1153", "Finn Madsen", 83.2);
		opretPatient("050972-1233", "Hans Joergensen", 89.4);
		opretPatient("011064-1522", "Ulla Nielsen", 59.9);
		opretPatient("090149-2529", "Ib Hansen", 87.7);

		opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
	}

}