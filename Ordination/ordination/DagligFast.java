package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	/**
	 * Attributes
	 */
	private LocalDate startDato;
	private LocalDate slutDato;
	private Patient patient;
	private int MAX_CAPACITY = 4;
	private Dosis[] doser = new Dosis[MAX_CAPACITY];

	/**
	 * Constructor To create an instance of DagligFast ordination
	 * 
	 * @param startDato
	 * @param slutDato
	 * @param patient
	 */
	public DagligFast(LocalDate startDato, LocalDate slutDato, Patient patient) {
		super(startDato, slutDato, patient);
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.patient = patient;
	}

	/**
	 * Method to create a dose
	 * 
	 * @param morgenAntal
	 * @param middagAntal
	 * @param aftenAntal
	 * @param natAntal
	 */
	public void createDosis(double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		doser[0] = new Dosis(LocalTime.of(8, 00), morgenAntal);
		doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
		doser[2] = new Dosis(LocalTime.of(18, 00), aftenAntal);
		doser[3] = new Dosis(LocalTime.of(00, 00), natAntal);
	}

	/**
	 * Method to get all doses
	 * 
	 * @return Dosis[] with all doses
	 */
	public Dosis[] getDoser() {
		Dosis[] result = new Dosis[MAX_CAPACITY];
		for (int i = 0; i < doser.length; i++) {
			result[i] = doser[i];
		}

		return result;
	}

	/**
	 * Method to get the total sum of doses
	 */
	@Override
	public double samletDosis() {
		double totalDosis = 0;
		for (int i = 0; i < this.antalDage(); i++) {
			Dosis[] dagsDoser = getDoser();
			for (int j = 0; j < this.MAX_CAPACITY; j++) {
				if(dagsDoser[j] != null) {
					totalDosis += dagsDoser[j].getAntal();
				}
			}
		}
		return totalDosis;
	}

	/**
	 * 
	 */
	@Override
	public double doegnDosis() {
		return doser.length;
	}

	public LocalDate getStartDato() {
		return startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}

	public Patient getPatient() {
		return patient;
	}

	public String getType() {
		return "DagligFast";
	}

}
