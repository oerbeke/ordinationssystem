package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	/**
	 * Attributes
	 */
	private LocalDate startDato;
	private LocalDate slutDato;
	private Patient patient;

	// Composition DagligSkaev 1 -> 0..* Dosis
	private ArrayList<Dosis> doser;

	/**
	 * Constructor
	 * 
	 * @param startDato
	 * @param slutDato
	 * @param patient
	 */
	public DagligSkaev(LocalDate startDato, LocalDate slutDato, Patient patient) {
		super(startDato, slutDato, patient);
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.patient = patient;
		this.doser = new ArrayList<Dosis>();
	}

	/**
	 * Method to create a new instance of Dagligskaev ordination dose
	 * @param tid
	 * @param antal
	 * @return newDosis
	 */
	public Dosis opretDosis(LocalTime tid, double antal) {
		Dosis newDosis = new Dosis(tid, antal);
		doser.add(newDosis);
		return newDosis;
	}

	/**
	 * Method to calculate the sum of all doses
	 */
	@Override
	public double samletDosis() {
		double totalDosis = 0;
		for (int i = 0; i < this.antalDage(); i++) {
			for (int j = 0; j < doser.size(); j++) {
				totalDosis += doser.get(j).getAntal();
			}
		}
		return totalDosis;
	}

	/**
	 * Method to return daily amount of doses
	 */
	@Override
	public double doegnDosis() {
		return doser.size();
	}

	/**
	 * Method to get all doses
	 * @return arrayList doses
	 */
	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}
	
	/**
	 * Getters and Setters
	 */
	public LocalDate getStartDato() {
		return startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}

	public Patient getPatient() {
		return patient;
	}

	public String getType() {
		return "DagligSkaev";
	}

}
