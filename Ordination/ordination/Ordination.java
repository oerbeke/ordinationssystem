package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {
	private LocalDate startDen;
	private LocalDate slutDen;
	private Laegemiddel laegemiddel;

	public Ordination(LocalDate startDen, LocalDate slutDen, Patient patient) {
		this.startDen = startDen;
		this.slutDen = slutDen;
		patient.addOrdination(this);
	}

	public LocalDate getStartDen() {
		return startDen;
	}

	public LocalDate getSlutDen() {
		return slutDen;
	}

	/**
	 * Hver opm�rksom p� at metoden kan returnere null, hvis laegemiddel er null.
	 * 
	 * @return
	 */
	public Laegemiddel getLaegemiddel() {
		return laegemiddel;
	}

	/**
	 * S�tter laegemiddel attributten til laegemiddel parameteren, s�fremt
	 * laegemiddel skal v�re null, s� bruges null i parameteren.
	 * 
	 * @param laegemiddel
	 */
	public void setLaegemiddel(Laegemiddel laegemiddel) {
		if (this.laegemiddel != laegemiddel) {
			this.laegemiddel = laegemiddel;
		}
	}

	/**
	 * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
	 * 
	 * @return antal dage ordinationen gaelder for
	 */
	public int antalDage() {
		return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
	}

	@Override
	public String toString() {
		return startDen.toString();
	}

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
	 * 
	 * @return
	 */
	public double samletDosis() {
		double totalDosis = 0;
		return totalDosis;
	}

	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen
	 * er gyldig
	 * 
	 * @return
	 */
	public double doegnDosis() {
		double doegnDosis = 0;
		doegnDosis = samletDosis() / this.antalDage();
		return doegnDosis;
	}

	/**
	 * Returnerer ordinationstypen som en String
	 * 
	 * @return
	 */
	public String getType() {
		return laegemiddel.getEnhed();
	}

	// ------------------------------------------------

}
