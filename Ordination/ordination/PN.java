package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

	/**
	 * Attributes
	 */
	private double antalEnheder;
	private LocalDate startDato;
	private LocalDate slutDato;
	private Patient patient;
	private ArrayList<LocalDate> dosisGivet;


	/**
	 * Constructor
	 * 
	 * @param patient
	 * @param startDen
	 * @param slutDen
	 * @param antalEnheder
	 */
	public PN(Patient patient, LocalDate startDato, LocalDate slutDato, double antalEnheder) {
		super(startDato, slutDato, patient);
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.antalEnheder = antalEnheder;
		this.patient = patient;
		this.dosisGivet = new ArrayList<>();
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if ((givesDen.isAfter(startDato) || givesDen.isEqual(startDato))
				&& (givesDen.isBefore(slutDato) || givesDen.isEqual(slutDato))) {
			dosisGivet.add(givesDen);
			return true;
		}
		return false;
	} 

	/**
	 * Method to calculate the daily dose
	 */
	@Override
	public double doegnDosis() {
		double doegnDosis = 0;
		doegnDosis = this.samletDosis() / this.antalDage();
		return doegnDosis;
	}

	/**
	 * Method to get the total amount of doses
	 */
	@Override
	public double samletDosis() {
		return dosisGivet.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int dosisGivet() {
		return dosisGivet.size();
	}

	/**
	 * Getters and Setters
	 */
	public double getAntalEnheder() {
		return antalEnheder;
	}
	
	public LocalDate getStartDato() {
		return startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}

	public Patient getPatient() {
		return patient;
	}
	
	public String getType() {
		return "PN";
	}

}
