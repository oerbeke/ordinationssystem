package storage;

import java.util.ArrayList;
import java.util.List;

import ordination.Laegemiddel;
import ordination.Patient;

public class Storage {
	private static Storage storage;
	private static Storage testStorage;
	private List<Patient> patienter = new ArrayList<Patient>();
	private List<Laegemiddel> laegemidler = new ArrayList<Laegemiddel>();

	private Storage() { }

	public static Storage getStorage() {
		if (storage == null) {
			storage = new Storage();
		}
		
		return storage;
	}
	
	public static Storage getTestStorage() {
		if (testStorage == null) {
			testStorage = new Storage();
		}
		
		return testStorage;
	}
	/**
	 * Returnerer en liste med alle gemte patienter
	 */
	public List<Patient> getAllPatienter() {
		return new ArrayList<Patient>(patienter);
	}

	/**
	 * Gemmer patient
	 */
	public void addPatient(Patient patient) {
		if (!patienter.contains(patient)) {
			patienter.add(patient);
		}
	}

	/**
	 * Returnerer en liste med alle gemte lægemidler
	 */
	public List<Laegemiddel> getAllLaegemidler() {
		return new ArrayList<Laegemiddel>(laegemidler);
	}

	/**
	 * Gemmer lægemiddel
	 */
	public void addLaegemiddel(Laegemiddel laegemiddel) {
		if (!laegemidler.contains(laegemiddel)) {
			laegemidler.add(laegemiddel);
		}
	}

}
