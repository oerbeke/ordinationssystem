package test;


import static org.junit.Assert.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class TestController {
	
	private static Controller tester = Controller.getTestController();
	private static Storage storage = Storage.getStorage();
	
	@Before
	public void setup() {
		tester.opretPatient("121256-0512", "Jane Jensen", 63.4);
		tester.opretPatient("070985-1153", "Finn Madsen", 83.2);
		tester.opretPatient("050972-1233", "Hans Joergensen", 89.4);
		tester.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
		tester.opretPatient("090149-2529", "Ib Hansen", 87.7);

		tester.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		tester.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		tester.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		tester.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		tester.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		tester.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		tester.opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		tester.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		tester.opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		tester.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an); 
	}
	
	/**
	 * metode OpretPnOrdination
	 */
	
	@Test
	public void tc1OpretPnOrdination() {
		// TestCase 1
		PN testcase1 = tester.opretPNOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 5), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 0);
		assertEquals(null, testcase1);
	}
	
	@Test
	public void tc2OpretPnOrdination() {
		// TestCase 2
		PN testcase1 = tester.opretPNOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 5), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 1);
		assertSame(testcase1, testcase1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void tc3OpretPnOrdination() {
		// TestCase 3
		PN testcase1 = tester.opretPNOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(1999, 12, 31), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 1);
	}
	
	/**
	 * Metode opretDagligFastOrdination
	 */
	@Test
	public void tc1OpretDagligFastOrdination() {
		DagligFast df = tester.opretDagligFastOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 5),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 0, 0, 0, 0);
		assertSame(df, df);
	}
	
	@Test
	public void tc2OpretDagligFastOrdination() {
		DagligFast df = tester.opretDagligFastOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 5),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 1, 1, 1, 1);
		assertSame(df, df);
	}	
	
	
	@Test(expected = IllegalArgumentException.class)
	public void tc3OpretDagligFastOrdination() {
		DagligFast df = tester.opretDagligFastOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(1999, 12, 31),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), -1, -1, -1, -1);
		
	}
	
	/**
	 * Metode opretDagligSkaevOrdination
	 */
	
	@Test
	public void tc1OpretDagligSkaevOrdination() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		DagligSkaev ds = tester.opretDagligSkaevOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 5),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
		assertSame(ds, ds);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void tc2OpretDagligSkaevOrdination() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5 };

		DagligSkaev ds = tester.opretDagligSkaevOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 5),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), kl, an);
	}
	
	/**
	 * Metode ordinationPnAnvendt
	 */
	@Test(expected = IllegalArgumentException.class)
	public void tc1OrdinationPNAnvendt() {
		PN test = tester.opretPNOrdination(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 5), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);
		
		LocalDate dato = LocalDate.of(1999, 12, 31);
		tester.ordinationPNAnvendt(test, dato);
	}
	
	
	/**
	 * Metode AnbefaletDosisPrDoegn
	 */
	@Test
	public void tc1AnbefaletDosisPrDoegn() {
		Patient nyPatient = tester.opretPatient("121256-0512", "Jane Jensen", 24);
		Laegemiddel middel = tester.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		
		assertEquals(2.4, tester.anbefaletDosisPrDoegn(nyPatient, middel), 0.001);
	}
	
	@Test
	public void tc2AnbefaletDosisPrDoegn() {
		Patient nyPatient = tester.opretPatient("121256-0512", "Jane Jensen", 25);
		Laegemiddel middel = tester.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		
		assertEquals(3.75, tester.anbefaletDosisPrDoegn(nyPatient, middel), 0.001);
	}
	
	@Test
	public void tc3AnbefaletDosisPrDoegn() {
		Patient nyPatient = tester.opretPatient("121256-0512", "Jane Jensen", 120);
		Laegemiddel middel = tester.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		
		assertEquals(18, tester.anbefaletDosisPrDoegn(nyPatient, middel), 0.001);
	}
	
	@Test
	public void tc4AnbefaletDosisPrDoegn() {
		Patient nyPatient = tester.opretPatient("121256-0512", "Jane Jensen", 121);
		Laegemiddel middel = tester.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		
		assertEquals(19.36, tester.anbefaletDosisPrDoegn(nyPatient, middel), 0.001);
	}
	
}
