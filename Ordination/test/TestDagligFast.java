package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Patient;

public class TestDagligFast {
	private DagligFast ds;

	@Before
	public void setUp() throws Exception {
		// Arrange
		Patient patient = new Patient("1234569876", "Kasper Henriksen", 80);
		LocalDate startDate = LocalDate.of(2000, 2, 7);
		LocalDate slutDate = LocalDate.of(2000, 2, 13);
		ds = new DagligFast(startDate, slutDate, patient);
	}

	@Test
	public void testCreateDosis1() {
		// Arrange
		// Act
		ds.createDosis(1, 1, 1, 1);
		Dosis[] expected = ds.getDoser();
		// Assert
		assertArrayEquals(expected, ds.getDoser());
	}

	@Test
	public void testCreateDosis2() {
		// Arrange
		// Act
		ds.createDosis(0, 0, 1, 0);
		Dosis[] expected = ds.getDoser();
		// Assert
		assertArrayEquals(expected, ds.getDoser());
	}

	@Test
	public void testCreateDosis3() {
		// Arrange
		// Act
		ds.createDosis(0, 0, 0, 0);
		Dosis[] expected = ds.getDoser();
		// Assert
		assertArrayEquals(expected, ds.getDoser());
	}

	@Test
	public void testGetDoser() {
		// Arrange
		// Act
		ds.createDosis(1, 2, 3, 4);
		Dosis[] expected = ds.getDoser();
		// Assert
		assertArrayEquals(expected, ds.getDoser());
	}

	@Test
	public void testSamletDosis1() {
		// Arrange
		Patient patient = new Patient("1234569876", "Kasper Henriksen", 80);
		LocalDate startDate = LocalDate.of(2000, 1, 1);
		LocalDate slutDate = LocalDate.of(2000, 1, 10);
		DagligFast ds1 = new DagligFast(startDate, slutDate, patient);
		// Act
		ds1.createDosis(4, 1, 4, 2);
		double result = ds1.samletDosis();
		// Assert
		assertEquals(110, result, 0.001);
	}

	@Test
	public void testSamletDosis2() {
		// Arrange
		Patient patient = new Patient("1234569876", "Kasper Henriksen", 80);
		LocalDate startDate = LocalDate.of(2000, 1, 10);
		LocalDate slutDate = LocalDate.of(2000, 1, 10);
		DagligFast ds2 = new DagligFast(startDate, slutDate, patient);
		// Act
		ds2.createDosis(4, 1, 4, 2);
		double result = ds2.samletDosis();
		// Assert
		assertEquals(11, result, 0.001);
	}

	@Test
	public void testSamletDosis3() {
		// Arrange
		Patient patient = new Patient("1234569876", "Kasper Henriksen", 80);
		LocalDate startDate = LocalDate.of(2000, 1, 1);
		LocalDate slutDate = LocalDate.of(2000, 1, 20);
		DagligFast ds3 = new DagligFast(startDate, slutDate, patient);
		// Act
		ds3.createDosis(4, 1, 4, 2);
		double result = ds3.samletDosis();
		// Assert
		assertEquals(220, result, 0.001);
	}

	@Test
	public void testDoegnDosis() {
		// Arrange
		// Act
		int result = ds.getDoser().length;
		// Assert
		assertEquals(4, result);
	}
}
