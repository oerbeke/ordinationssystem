package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Patient;

public class TestDagligSkaev {
	private DagligSkaev ds, ds1, ds2;

	@Before
	public void setUp() throws Exception {
		// Arrange
		Patient patient = new Patient("1234569876", "Kasper Henriksen", 80);
		LocalDate startDate = LocalDate.of(2000, 2, 7);
		LocalDate slutDate = LocalDate.of(2000, 2, 13);
		ds = new DagligSkaev(startDate, slutDate, patient);
		ds1 = new DagligSkaev(startDate, slutDate, patient);
		ds2 = new DagligSkaev(startDate, slutDate, patient);
	}

	@Test
	public void testOpretDosis1() {
		// Arrange
		LocalTime time = LocalTime.of(10, 15);
		// Act
		ds.opretDosis(time, 20);
		// Assert
		assertEquals(1, ds.getDoser().size());
		assertEquals(20, ds.getDoser().get(0).getAntal(), 0.001);
		assertEquals(time, ds.getDoser().get(0).getTid());
	}

	@Test
	public void testOpretDosis2() {
		// Arrange
		LocalTime time = LocalTime.of(20, 30);
		// Act
		ds.opretDosis(time, 5);
		// Assert
		assertEquals(1, ds.getDoser().size());
		assertEquals(5, ds.getDoser().get(0).getAntal(), 0.001);
		assertEquals(time, ds.getDoser().get(0).getTid());
	}

	@Test
	public void testOpretDosis3() {
		// Arrange
		LocalTime time = LocalTime.of(8, 20);
		// Act
		ds.opretDosis(time, 2.2);
		// Assert
		assertEquals(1, ds.getDoser().size());
		assertEquals(2.2, ds.getDoser().get(0).getAntal(), 0.001);
		assertEquals(time, ds.getDoser().get(0).getTid());
	}

	@Test
	public void testOpretDosis4() {
		// Arrange
		LocalTime time = LocalTime.of(5, 45);
		// Act
		ds.opretDosis(time, 10);
		// Assert
		assertEquals(1, ds.getDoser().size());
		assertEquals(10, ds.getDoser().get(0).getAntal(), 0.001);
		assertEquals(time, ds.getDoser().get(0).getTid());
	}

	@Test
	public void testSamletDosis1() {
		// Arrange
		LocalTime time = LocalTime.of(5, 45);
		// Act
		ds.opretDosis(time, 0);
		// Assert
		assertEquals(0, ds.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosis2() {
		// Arrange
		LocalTime time = LocalTime.of(5, 45);
		// Act
		ds1.opretDosis(time, 10);
		// Assert
		assertEquals(70, ds1.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosis3() {
		// Arrange
		LocalTime time = LocalTime.of(5, 45);
		// Act
		ds2.opretDosis(time, 10);
		ds2.opretDosis(time, 10);
		ds2.opretDosis(time, 10);
		ds2.opretDosis(time, 10);
		ds2.opretDosis(time, 10);
		// Assert
		assertEquals(350, ds2.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis1() {
		// Arrange
		// Act
		// Assert
		assertEquals(0, ds.getDoser().size(), 0.001);
	}

	@Test
	public void testDoegnDosis2() {
		// Arrange
		LocalTime time = LocalTime.of(5, 45);
		// Act
		ds.opretDosis(time, 10);
		ds.opretDosis(time, 10);
		ds.opretDosis(time, 10);
		ds.opretDosis(time, 10);
		ds.opretDosis(time, 10);
		// Assert
		assertEquals(5, ds.getDoser().size(), 0.001);
	}

}
