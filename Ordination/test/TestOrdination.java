package test;

import static org.junit.Assert.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.Patient;

public class TestOrdination {

	 private static Patient patient = new Patient("121256-0512", "Jane Jensen", 63.4);
	 private static Ordination o1 = new Ordination(LocalDate.of(2000, Month.FEBRUARY, 1), LocalDate.of(2000, Month.FEBRUARY, 10), patient) {};
	 private static Ordination o2 = new Ordination(LocalDate.of(2000, Month.FEBRUARY, 1), LocalDate.of(2000, Month.FEBRUARY, 10), patient) {};
	 private static Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml"); 
	 private static Laegemiddel l2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml"); 
	 
	 
	@Before
	public void setup() {
		patient.addOrdination(o1);
		o1.setLaegemiddel(l1);
		
	}
	
	
	@Test
	public void tc1() {
		assertSame(l1, o1.getLaegemiddel());
		
	}
}
