package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ordination.PN;
import ordination.Patient;

public class testPN {
	
	private PN testPN;
	private PN testPN1;
	private PN testPN2;
	private PN testPN3;
	private PN testPN4;
	private PN testPN5;
	private PN testPN6;
	private Patient finn;
	private LocalDate startDato;
	private LocalDate slutDato;
	
	/** samletDosis givet datoer **/
	private LocalDate d1 = LocalDate.of(2000, Month.FEBRUARY, 1);
	private LocalDate d2 = LocalDate.of(2000, Month.FEBRUARY, 5);
	private LocalDate d3 = LocalDate.of(2000, Month.FEBRUARY, 10);
	private LocalDate d4 = LocalDate.of(2000, Month.FEBRUARY, 15);
	private LocalDate d5 = LocalDate.of(2000, Month.FEBRUARY, 20);
	private LocalDate d6 = LocalDate.of(2000, Month.MARCH, 1);
	private LocalDate d7 = LocalDate.of(2000, Month.MARCH, 10);
	private LocalDate d8 = LocalDate.of(2000, Month.MARCH, 25);
	private LocalDate d9 = LocalDate.of(2000, Month.APRIL, 29);
	private LocalDate d10 = LocalDate.of(2000, Month.MAY, 5);
	
	/** DoegnDosis givet datoer **/
	private LocalDate dd1 = LocalDate.of(2000, Month.FEBRUARY, 1);
	private LocalDate dd2 = LocalDate.of(2000, Month.FEBRUARY, 2);
	private LocalDate dd3 = LocalDate.of(2000, Month.FEBRUARY, 3);
	private LocalDate dd4 = LocalDate.of(2000, Month.FEBRUARY, 4);
	private LocalDate dd5 = LocalDate.of(2000, Month.FEBRUARY, 5);
	private LocalDate dd6 = LocalDate.of(2000, Month.FEBRUARY, 6);
	private LocalDate dd7 = LocalDate.of(2000, Month.FEBRUARY, 7);
	private LocalDate dd8 = LocalDate.of(2000, Month.FEBRUARY, 8);
	private LocalDate dd9 = LocalDate.of(2000, Month.FEBRUARY, 9);
	private LocalDate dd10 = LocalDate.of(2000, Month.FEBRUARY, 10);
	
	
	@Before
	public void setup() {
		startDato = LocalDate.of(2000, Month.JANUARY, 28);
		slutDato = LocalDate.of(2000, Month.JULY, 28);
		finn = new Patient("070985-1153", "Finn Madsen", 83.2);
		testPN = new PN(finn, startDato, slutDato, 20);
		testPN1 = new PN(finn, startDato, slutDato, 20);
		testPN2 = new PN(finn, startDato, slutDato, 0);
		testPN3 = new PN(finn, startDato, slutDato, 5.4);
		
		testPN1.givDosis(d1);
		testPN1.givDosis(d2);
		testPN1.givDosis(d3);
		testPN1.givDosis(d4);
		testPN1.givDosis(d5);
		testPN1.givDosis(d6);
		testPN1.givDosis(d7);
		testPN1.givDosis(d8);
		testPN1.givDosis(d9);
		testPN1.givDosis(d10);
		
		testPN3.givDosis(d1);
		testPN3.givDosis(d2);
		testPN3.givDosis(d3);
		testPN3.givDosis(d4);
		testPN3.givDosis(d5);
		testPN3.givDosis(d6);
		testPN3.givDosis(d7);
		testPN3.givDosis(d8);
		testPN3.givDosis(d9);
		testPN3.givDosis(d10);
		
		LocalDate startden = LocalDate.of(2000, Month.FEBRUARY, 1);
		LocalDate slutden = LocalDate.of(2000, Month.FEBRUARY, 10);
		testPN4 = new PN(finn, startden, slutden, 10);
		
		testPN4.givDosis(dd1);
		testPN4.givDosis(dd2);
		testPN4.givDosis(dd3);
		testPN4.givDosis(dd4);
		testPN4.givDosis(dd5);
		testPN4.givDosis(dd6);
		testPN4.givDosis(dd7);
		testPN4.givDosis(dd8);
		testPN4.givDosis(dd9);
		testPN4.givDosis(dd10);
		
		testPN5 = new PN(finn, startden, slutden, 2);
		
		testPN5.givDosis(dd1);
		testPN5.givDosis(dd2);
		testPN5.givDosis(dd3);
		testPN5.givDosis(dd4);
		testPN5.givDosis(dd5);
		
		testPN6 = new PN(finn, startden, slutden, 2);
		
		testPN6.givDosis(dd1);
		testPN6.givDosis(dd2);
		testPN6.givDosis(dd3);
		testPN6.givDosis(dd4);
		
	}
	
	/**
	 * Test af metode givDosis
	 */
	@Test
	public void tc1() {
		// dosisGivet
		assertEquals(0, testPN.dosisGivet(), 0.001);
		
		LocalDate givesDen = LocalDate.of(2000, Month.JANUARY, 27);
		// givesDen
		assertFalse(testPN.givDosis(givesDen));
		
		// forventet size = 0
		assertEquals(0, testPN.dosisGivet(), 0.001);
		
	}
	
	@Test
	public void tc2() {
		// dosisGivet
		assertEquals(0, testPN.dosisGivet(), 0.001);
		
		LocalDate givesDen = LocalDate.of(2000, Month.JANUARY, 28);
		// givesDen
		assertTrue(testPN.givDosis(givesDen));
		
		// forventet size = 1
		assertEquals(1, testPN.dosisGivet(), 0.001);
	}
	
	
	@Test
	public void tc3() {
		// dosisGivet
		assertEquals(0, testPN.dosisGivet(), 0.001);
		
		LocalDate givesDen = LocalDate.of(2000, Month.MAY, 20);
		// givesDen
		assertTrue(testPN.givDosis(givesDen));
		
		// forventet size = 1
		assertEquals(1, testPN.dosisGivet(), 0.001);
	}
	
	
	@Test
	public void tc4() {
		// dosisGivet
		assertEquals(0, testPN.dosisGivet(), 0.001);
		
		LocalDate givesDen = LocalDate.of(2000, Month.JULY, 28);
		// givesDen
		assertTrue(testPN.givDosis(givesDen));
		
		// forventet size = 1
		assertEquals(1, testPN.dosisGivet(), 0.001);
	}
	
	@Test
	public void tc5() {
		// dosisGivet
		assertEquals(0, testPN.dosisGivet(), 0.001);
		
		LocalDate givesDen = LocalDate.of(2000, Month.JULY, 29);
		// givesDen
		assertFalse(testPN.givDosis(givesDen));
		
		// forventet size = 0
		assertEquals(0, testPN.dosisGivet(), 0.001);
		
	}
	
	/**
	 * Test af metode samletDosis
	 */
	@Test
	public void tc1SamletDosis() {
		// antalEnheder
		assertEquals(20, testPN1.getAntalEnheder(), 0.001);
		// forventet
		assertEquals(200, testPN1.samletDosis(), 0.001);
	}
	
	@Test
	public void tc2SamletDosis() {
		// antalEnheder
		assertEquals(0, testPN2.getAntalEnheder(), 0.001);
		// forventet
		assertEquals(0, testPN2.samletDosis(), 0.001);
	}
	
	@Test
	public void tc3SamletDosis() {
		// antalEnheder
		assertEquals(5.4, testPN3.getAntalEnheder(), 0.001);
		// forventet
		assertEquals(54, testPN3.samletDosis(), 0.001);
	}
	
	/**
	 * Test af metode doegnDosis
	 */
	@Test
	public void tc1DoegnDosis() {
		// antalDage
		assertEquals(10, testPN4.antalDage(), 0.001);
		// samletDosis
		assertEquals(100, testPN4.samletDosis(), 0.001);
		// doegnDosis - forventet retur = 10
		assertEquals(10, testPN4.doegnDosis(), 0.001);
	}
	
	@Test
	public void tc2DoegnDosis() {
		// antalDage
		assertEquals(10, testPN5.antalDage(), 0.001);
		// samletDosis
		assertEquals(10, testPN5.samletDosis(), 0.001);
		// doegnDosis - forventet retur = 7.55
		assertEquals(1, testPN5.doegnDosis(), 0.001);
	}
	
	@Test
	public void tc3DoegnDosis() {
		// antalDage
		assertEquals(10, testPN6.antalDage(), 0.001);
		// samletDosis
		assertEquals(8, testPN6.samletDosis(), 0.001);
		// doegnDosis - forventet retur = 7.55
		assertEquals(0.8, testPN6.doegnDosis(), 0.001);
	}
}
